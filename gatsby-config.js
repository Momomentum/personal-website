/**
 * @type {import('gatsby').GatsbyConfig}
 */
module.exports = {
  siteMetadata: {
    title: `Momomentum`,
    siteUrl: `https://www.yourdomain.tld`,
  },
  plugins: [],
}
